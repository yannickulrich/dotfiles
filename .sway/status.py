#!/usr/bin/python
import subprocess
import re
import json
import time
import sys
import threading
import os
import signal
import dbus
import logging
from systemd.journal import JournalHandler
import mpris_daemon


class Widget:
    def __init__(self):
        self.log = logging.getLogger('swaybar.' + self.name)
        self.log.addHandler(JournalHandler())
        self.log.setLevel(logging.WARN)

    def __call__(self):
        pass

    def click(self, x, y):
        pass
    def rclick(self, x, y):
        pass
    def mclick(self, x, y):
        pass

    def __call__(self):
        try:
            dic = {"full_text": self.text()}
        except Exception as e:
            self.log.error(f"Write thread encountered an error for {self.name}", exc_info=e)
            dic = {"full_text": ""}

        if hasattr(self, "name"):
            dic["name"] = self.name
        if hasattr(self, "align"):
            dic["align"] = self.align
        dic["min_width"] = self.min_width

        return dic


class Power(Widget):
    name = "power"
    min_width = 20
    align = "right"
    device = "/org/freedesktop/UPower/devices/battery_BAT1"
    old_state = 0
    low_battery_warned = False
    warning_level = 20

    def __init__(self):
        self.bus = dbus.SystemBus()
        super().__init__()

        proc = subprocess.Popen(['brightnessctl', 'm'], stdout=subprocess.PIPE)
        self.brightness_step = int(proc.communicate()[0]) / 5

    def text(self):
        proxy = self.bus.get_object("org.freedesktop.UPower", self.device)
        iface = dbus.Interface(proxy, "org.freedesktop.DBus.Properties")

        state = int(iface.Get("org.freedesktop.UPower.Device","State"))
        level = int(iface.Get("org.freedesktop.UPower.Device","Percentage"))

        if self.old_state != state:
            self.log.info(f"Changed {self.old_state} -> {state}, level: {level}")
            self.old_state = state

        if state == 4: # "fully-charged"
            return f"\U0001f50c {level}%"
        elif state == 1: # "charging"
            time = int(iface.Get(
                "org.freedesktop.UPower.Device","TimeToFull"
            )) // 60
            if level > self.warning_level:
                self.low_battery_warned = False

            return f"\u26a1 {level}% {time//60}h{time%60}"
        else:
            time = int(iface.Get(
                "org.freedesktop.UPower.Device","TimeToEmpty"
            )) // 60
            if level < self.warning_level and not self.low_battery_warned:
                self.low_battery_warned = True
                self.nag = subprocess.Popen(['swaynag', '--message', f'Battery level is {level}%, time remaining: {time//60}h{time%60}', '--layer', 'overlay'])

            return f"\U0001f50b {level}% {time//60}h{time%60}"

    def click(self, x, y):
        proc = subprocess.Popen(['brightnessctl', 'g'], stdout=subprocess.PIPE)
        brightness = int(proc.communicate()[0])

        new_step = int(brightness / self.brightness_step) + 1
        if new_step > 5:
            new_step = 1

        new = self.brightness_step * new_step
        self.log.warning(f"Brightness {brightness} -> {new}")
        subprocess.Popen(['brightnessctl', 's', str(new)], stdout=subprocess.PIPE)


class Audio(Widget):
    name = "audio"
    min_width = 85
    align = "right"

    _pid = -1

    def __init__(self):
        super().__init__()
        self.log.setLevel(logging.WARN)

        self.daemon = mpris_daemon.Daemon()

        self.mpris_thread = threading.Thread(
            target=self.daemon.run
        )
        self.mpris_thread.start()

    def volume(self):
        proc = subprocess.Popen([
            "pactl",
            "get-sink-mute",
            "@DEFAULT_SINK@"
        ], stdout=subprocess.PIPE)
        stdout = proc.communicate()[0]
        if b"yes" in stdout:
            return '\U0001f507'

        proc = subprocess.Popen([
            "pactl",
            "get-sink-volume",
            "@DEFAULT_SINK@"
        ], stdout=subprocess.PIPE)
        stdout = proc.communicate()[0].decode()
        vol = re.findall("(\d*)%", stdout)
        if len(vol) == 0:
            return ""

        vol = int(vol[0])

        if vol < 30:
            icon = "\U0001f508"
        elif vol < 60:
            icon = "\U0001f509"
        else:
            icon = "\U0001f50a"

        return f"{icon} {vol}%"

    def playing(self):
        if any(i.playing for i in self.daemon.children):
            return "\u23F8"
        else:
            return "\u25B6"

    def text(self):
        return self.playing() + "  " + self.volume()

    def click(self, x, y):
        self.daemon.toggle()


class CPU(Widget):
    name = "cpu"
    min_width = 75

    stat = None
    def text(self):
        with open("/proc/stat") as fp:
            stat = [int(i) for i in re.findall(
                "cpu +(\d*) (\d*) (\d*) (\d*) (\d*)", fp.read()
            )[0]]

        if self.stat:
            # usr, nice, sys, idle, iowait
            diff = [i-j for i,j in zip(stat, self.stat)]
            active = diff[0] + diff[2] + diff[4]
            total = diff[0] + diff[2] + diff[3] + diff[4]

            usage = active / total * 100

            self.stat = stat
            return f"\U0001F914 {usage:.1f}%"
        else:
            self.stat = stat
            return "\U0001F914 N/A"


class Network(Widget):
    name = "network"
    min_width = 110

    def __init__(self):
        self.bus = dbus.SystemBus()
        super().__init__()
        self.log.setLevel(logging.INFO)

    def text(self):
        proxy = self.bus.get_object(
            "org.freedesktop.NetworkManager", "/org/freedesktop/NetworkManager"
        )
        iface = dbus.Interface(proxy, "org.freedesktop.DBus.Properties")
        active = iface.Get(
            "org.freedesktop.NetworkManager", "ActiveConnections"
        )

        ans = ""
        self.map = []

        for con in active:
            proxy = self.bus.get_object('org.freedesktop.NetworkManager', con)
            iface = dbus.Interface(
                proxy, dbus_interface='org.freedesktop.DBus.Properties'
            )
            id = str(iface.Get(
                "org.freedesktop.NetworkManager.Connection.Active", "Id"
            ))
            type = iface.Get(
                "org.freedesktop.NetworkManager.Connection.Active", "Type"
            )
            state = int(iface.Get(
                "org.freedesktop.NetworkManager.Connection.Active","State"
            ))
            # log.info(f"Network device: {id} ({str(type)}) at {str(con)}")

            if state != 2:
                if len(id) > 8:
                    ent = f"\U0001D13D {id[:5]}\u2026"
                else:
                    ent = f"\U0001D13D {id:8s}"
            elif type == "802-11-wireless":
                if len(id) > 8:
                    ent = f"\U0001f4f6 {id[:5]}\u2026"
                else:
                    ent = f"\U0001f4f6 {id:8s}"
            elif type == "802-3-ethernet":
                ent = "\U0001f517"
            elif type == "mobile":
                ent = "\U0001f6f0"
            else:
                continue

            ans += ent + " "
            self.map.append((len(ans), con, type))

        return ans[:-1]


    def click(self, x, y):
        xx = round(x * 0.11 - 0.96)
        for x0, con, type in self.map:
            if x > x0:
                self.log.info(f"Clicked on {str(con)} ({type})")
                break


class Date(Widget):
    name = "date"
    align = "center"

    def __init__(self, w):
        self.w = w
        super().__init__()

    @property
    def min_width(self):
        tot = sum(
            i.min_width
            for i in self.root.children
            if i.name != "date"
        )
        return self.w - tot

    fmt = "%a, %d %b  %I:%M:%S %p"
    def text(self):
        return time.strftime(self.fmt)


class Squeekboard(Widget):
    name = "squeekboard"
    min_width = 25
    addr = ["--user", "sm.puri.OSK0", "/sm/puri/OSK0", "sm.puri.OSK0"]
    def __init__(self):
        super().__init__()
        self.start()

    def start(self):
        self.proc = subprocess.Popen(
            "/usr/bin/squeekboard",
            stdout=subprocess.PIPE, stderr=subprocess.PIPE
        )
        self.log.info("Started squeekboard")

    def text(self):
        return "\u2328"

    def click(self, x, y):
        self.log.info("Clicked!")
        if self.proc is not None:
            self.log.warning("squeekboard died, restarting")
            self.start()

        proc = subprocess.Popen(
            ["busctl", "get-property"] + self.addr + ["Visible"],
            stdout=subprocess.PIPE
        )
        self.state = b"false" in proc.communicate()[0]
        self.log.warning(f"Setting state to {self.state}")
        proc = subprocess.Popen(
            ["busctl", "call"] + self.addr + ["SetVisible", "b", "true" if self.state else "false"],
            stdout=subprocess.PIPE
        )


class Launcher(Widget):
    min_width = 25
    def __init__(self, name, icon, app=None):
        self.name = name
        self.icon = icon
        if app is None:
            self.app = ["/usr/bin/" + name]
        elif isinstance(app, str):
            self.app = [app]
        else:
            self.app = app

        super().__init__()
        self.log.setLevel(logging.INFO)

    def text(self):
        return self.icon

    def click(self, x, y):
        self.log.info("Starting app " + " ".join(self.app))
        subprocess.Popen(["sway", "exec", " ".join(self.app)], stdout=subprocess.PIPE)


class Lock(Widget):
    name = "lock"
    min_width = 25

    def text(self):
        return "\U0001F512"

    def click(self, x, y):
        proc = subprocess.Popen(["pidof", "swayidle"], stdout=subprocess.PIPE)
        stdout = proc.communicate()[0]

        if stdout:
            pid = int(stdout)
            self.log.warning(f"Suspending using pid {pid}")
            os.kill(pid, signal.SIGUSR1)
        else:
            self.log.error("Can't find swayidle")


class StatusBar:
    timeout = 1
    def __init__(self, *args):
        self.log = logging.getLogger('swaybar')
        self.log.addHandler(JournalHandler())
        self.log.setLevel(logging.INFO)

        self.children = args
        for i in self.children:
            i.root = self

        header = json.dumps({
            "version": 1,
            "click_events": True
        })

        sys.stdout.write(f"{header}\n[[]")

    def run1(self):
        try:
            return json.dumps([child() for child in self.children])
        except Exception as e:
            self.log.error("Write thread encountered an error", exc_info=e)
            return "[]"

    def write_thread(self):
        try:
            while True:
                sys.stdout.write("," + self.run1() + "\n")
                sys.stdout.flush()

                time.sleep(self.timeout)
        except Exception as e:
            self.log.error("Write thread died", exc_info=e)

    def read_thread(self):
        try:
            assert sys.stdin.readline() == "[\n"
            while True:
                l = sys.stdin.readline().strip()
                if len(l) == 0:
                    continue

                if l[0] == ',':
                    l = l[1:]

                dat = json.loads(l)
                for child in self.children:
                    if not hasattr(child, "name"):
                        continue
                    if child.name != dat["name"]:
                        continue

                    child.width = dat["width"]
                    child.height = dat["height"]

                    if dat["button"] == 1:
                        self.log.info(f"LClick on {child} at {dat['relative_x']},{dat['relative_y']}")
                        child.click(dat["relative_x"], dat["relative_y"])
                    elif dat["button"] == 2:
                        self.log.info(f"MClick on {child} at {dat['relative_x']},{dat['relative_y']}")
                        child.mclick(dat["relative_x"], dat["relative_y"])
                    elif dat["button"] == 3:
                        self.log.info(f"RClick on {child} at {dat['relative_x']},{dat['relative_y']}")
                        child.rclick(dat["relative_x"], dat["relative_y"])

        except Exception as e:
            self.log.error("Read thread died", exc_info=e)


if __name__ == "__main__":
    runner = StatusBar(
        Launcher("firefox", "\U0001f98a"),
        Launcher("mathematica", "\U0001f578","/usr/local/bin/mathematica"),
        Squeekboard(),
        Launcher("foot", "\U0001F680"),
        Launcher("com.github.johnfactotum.Foliate", "\U0001F4D6"),
        Launcher("rhythmbox", "\U0001F3A7"),
        Launcher("xournalpp", "\U0001F4DD"),
        Date(1050),
        CPU(), Network(), Audio(), Power(),
        Lock(),
        Launcher("suspend", "\u23FB", ["/usr/bin/systemctl", "suspend"]),
    )
    thread = threading.Thread(target=runner.read_thread)
    thread.start()
    runner.write_thread()
