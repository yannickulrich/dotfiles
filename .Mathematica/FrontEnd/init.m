SetOptions[$FrontEnd,
    ConversionOptions->{
        "ExportOptions"->{"EPS" -> {"IncludeSpecialFonts" -> True}},
        "ImportOptions"->{"Package" -> {"BlankLinesBetweenCells" -> 2}}
    },
    "DisplayImagePixels"->"Automatic",
    ScreenResolutionCompatibilityMode->False,
    PrivateFrontEndOptions->{"LicensesAgreed"->{"12.3"}},
    VersionsLaunched->{"12.3.1"},
    EvaluatorNames->{
        "Local" -> {"AutoStartOnLaunch" -> True},
        "persistent" -> {
            "RawMathLinkProgram" -> True,
            "AppendNameToCellLabel" -> True,
            "AutoStartOnLaunch" -> False,
            "Executable" -> "",
            "MLOpenArguments" -> "-LinkMode Connect -LinkProtocol TCPIP -LinkHost \"lcth32.psi.ch\" -LinkName \"8000@lcth32.psi.ch,8001@lcth32.psi.ch\""
        }
    },
    FindSettings->{},
    StyleNameDialogSettings->{"Style"->"Program"},
    PrivateFrontEndOptions->{
        "InterfaceSettings"->{
            "PredictiveInterface" -> {"ShowMinimized" -> False, "FirstUse" -> False}
        },
        "ShowAtStartup"->"NewDocument"
    },
    InputAliases->{
        "intt" -> RowBox[{"\[Integral]", RowBox[{"\[SelectionPlaceholder]", RowBox[{"\[DifferentialD]", "\[Placeholder]"}]}]}],
        "dintt"-> RowBox[{
            SubsuperscriptBox["\[Integral]", "\[SelectionPlaceholder]", "\[Placeholder]"],
            RowBox[{"\[Placeholder]", RowBox[{"\[DifferentialD]", "\[Placeholder]"}]}]
        }],
        "sumt" -> RowBox[{
            UnderoverscriptBox["\[Sum]", RowBox[{"\[SelectionPlaceholder]", "=", "\[Placeholder]"}], "\[Placeholder]"],
            "\[Placeholder]"
        }],
        "prodt" -> RowBox[{
            UnderoverscriptBox["\[Product]", RowBox[{"\[SelectionPlaceholder]", "=", "\[Placeholder]"}], "\[Placeholder]"],
            "\[Placeholder]"
        }],
        "gg" -> "\!\(\*SubscriptBox[\(\[DoubleStruckG]\), \(\[SelectionPlaceholder],\[Placeholder]\)]\)",
        "levi" -> "\!\(\*SubscriptBox[\(\[CurlyEpsilon]\), \(\[SelectionPlaceholder],\[Placeholder],\[Placeholder],\[Placeholder]\)]\)",
        "ub" -> "\[LeftAngleBracket]\[ScriptU][\[SelectionPlaceholder],\[Placeholder]]",
        "vb" -> "\[LeftAngleBracket]\[ScriptV][\[SelectionPlaceholder],\[Placeholder]]",
        "u" -> "\[ScriptU][\[SelectionPlaceholder],\[Placeholder]]\[RightAngleBracket]",
        "v" -> "\[ScriptV][\[SelectionPlaceholder],\[Placeholder]]\[RightAngleBracket]",
        "dim" -> "\[ScriptD]",
        "11" -> "\[DoubleStruckOne]",
        "PL" -> "\[DoubleStruckCapitalP]L",
        "PR" -> "\[DoubleStruckCapitalP]R",
        "mm" -> "\[Micro]"
    },
    InputAutoReplacements->{
        "==" -> "\[Equal]",
        "->" -> "\[Rule]",
        ":>" -> "\[RuleDelayed]",
        "<=" -> "\[LessEqual]",
        ">=" -> "\[GreaterEqual]",
        "!=" -> "\[NotEqual]"
    },
    ShowPredictiveInterface->False,
    CodeAssistOptions->{
        "AutoConvertEnable"->False
    },
    SpellingDictionaries->{
        "CorrectWords"->{
            "Renormalisation", "vacua", "parametrisation", "parametrise"
        }
    },
    StyleHints->Association["OperatorRenderings" -> {}, "GroupOpener" -> "Inline"],
    AutoMultiplicationSymbol->{"Numbers", "LineBreaks"},
    Magnification->0.75
]

