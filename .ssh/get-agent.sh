#!/bin/bash

sshagentid=$(pidof ssh-agent)
if [[ -z $sshagentid ]]; then
    ssh-agent
fi

found_agent=0
for i in /tmp/ssh-* ; do
    if [[ -O $i ]]; then
        for j in $i/* ; do
            SSH_AUTH_SOCK=$j ssh-add -l > /dev/null 2>&1
            errcode=$?
            if [[ "$errcode" != 2 ]]; then
                if [[ "$found_agent" != "0" ]]; then
                    echo "Found multiple agents"
                    echo "$found_agent"
                    echo "$j"
                else
                    found_agent=$j
                    SSH_AUTH_SOCK=$j $HOME/.ssh/rc
                    if [[ "$errcode" == 1 ]]; then
                        SSH_AUTH_SOCK=$j ssh-add
                    fi
                fi
            fi
        done
    fi
done
