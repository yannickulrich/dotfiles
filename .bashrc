# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

case `cat $HOME/.system` in
    ippp*|psi*|itpc*)
        if [[ "$0" == "-bash" ]]; then
            echo "new"
            exec zsh
        fi
        ;;
    itp*)
        if [[ "$0" == "-bash" ]]; then
            echo "new"
            exec $HOME/.local/bin/zsh
        fi
        ;;
esac

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|*-256color|alacritty|foot) color_prompt=yes;;
esac

if [ "$color_prompt" = yes ]; then
    case `cat $HOME/.system` in
        surface*)
            c1="32"
            c2="34"
            prefix=""
            ;;
        ippp*)
            c1="34"
            c2="32"
            prefix="(IPPP) "
            ;;
        itp*)
            c1="34"
            c2="32"
            prefix="(ITP) "
            ;;
        itpc*)
            c1="34"
            c2="32"
            prefix="(cluster) "
            ;;
        psi*)
            c1="34"
            c2="32"
            prefix="(PSI) "
            ;;
        phone*)
            c1="34"
            c2="32"
            prefix="(pine)"
    esac
    PS1="$prefix\[\033[01;${c1}m\]\u@\h\[\033[00m\]:\[\033[01;${c2}m\]\W\[\033[00m\]\$ "
else
    PS1='\u@\h:\W\$ '
fi
unset color_prompt

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# some more ls aliases
alias ll='ls -alFctr'
alias la='ls -A'
alias l='ls -CF'
alias bat='bat --paging=never'

# My stuff

export BAT_THEME="GitHub"

case `cat $HOME/.system` in
    surface*)
        export GST_PLUGIN_PATH=/usr/local/lib/x86_64-linux-gnu/gstreamer-1.0
        export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/lib:/usr/lib:/usr/local/lib:/usr/local/lib/x86_64-linux-gnu/
        export PYTHONPATH=$PYTHONPATH:/home/yannickulrich/mcmule/pymule
        export PATH=$HOME/.local/math12.3:$PATH:$HOME/.local/bin:$HOME/.cargo/bin
        alias xournal=xournalpp
        alias ssh='/home/yannickulrich/.ssh/ssh-wrapper'
        alias scp='scp -S /home/yannickulrich/.ssh/ssh-wrapper'
        export MAIN_TTY=\#{pane_tty}
        $HOME/.ssh/get-agent.sh

        if dropbox status | grep -q 'running' ; then
            dropbox start > /dev/null 2> /dev/null &
        fi
        ;;
    psi*)
        . /afs/psi.ch/project/muondecay/env.sh
        PATH=$HOME/.local/bin:$PATH
        hostname=`hostname`
        if [ "$hostname" == "lcth32" ] ||  [ "$hostname" == "lcth31" ] ||  [ "$hostname" == "lcth22" ] ||  [ "$hostname" == "lcth21" ]; then
            alias kr='kinit -k -t /scratch/ulrich/.tab.keytab ulrich_y@D.PSI.CH ; aklog'
            alias krs='k5reauth -i 3600 -p ulrich_y -k /scratch/ulrich/.tab.keytab -f -- screen'
        elif [ "$hostname" == "pc13590.psi.ch" ] || [ "$hostname" == "pc10634.psi.ch" ]; then
            export PATH=/scratch/ulrich/usr/bin:$PATH
            export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/scratch/ulrich/usr/lib
            export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:/scratch/ulrich/usr/lib/pkgconfig
            alias kr='kinit -k -t /scratch/ulrich/.tab.keytab ulrich_y@D.PSI.CH ; aklog'
            alias krs='k5reauth -i 3600 -p ulrich_y -k /scratch/ulrich/.tab.keytab -f -- screen'
        fi
        PATH=/afs/psi.ch/sys/psi.x86_64_slp6/Tools/git/2.33.1/bin:$PATH
        export MAIN_TTY=$SSH_TTY
        ;;
    ippp*)
        export MAIN_TTY=$SSH_TTY
        source /opt/rh/rh-python38/enable
        source /opt/rh/devtoolset-10/enable
        export PATH=$HOME/.local/bin/:$PATH
        ;;
    itp*)
        export MAIN_TTY=$SSH_TTY
        export PATH=$HOME/.local/bin/:$PATH
        ;;
esac

alias colourdiff=colordiff
export EDITOR=vim
alias config='git --git-dir=$HOME/.cfg/ --work-tree=$HOME'

alias clear="/usr/bin/clear -T tmux"


function gitignore (){
    IFS=","
    req="$*"
    curl "https://www.toptal.com/developers/gitignore/api/$req" > .gitignore
    git add .gitignore
}

# Best autocomplete ever!!
if [[ $- == *i* ]]
then
    # bind '"\e[A": history-search-backward'
    # bind '"\e[B": history-search-forward'
    [[ -f ~/.bash-preexec.sh ]] && source ~/.bash-preexec.sh
    source $HOME/.config/atuin/script.sh
fi

# End of my stuff
# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi
