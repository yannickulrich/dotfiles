#!/usr/bin/python
import dbus
from dbus.mainloop.glib import DBusGMainLoop
import gi
gi.require_version('Gst', '1.0')
from gi.repository import Gst, GLib
import logging
from systemd.journal import JournalHandler
import signal
import subprocess
import json


class IIOWatcher:
    bus_name = 'net.hadess.SensorProxy'

    def __init__(self, output):
        self.log = logging.getLogger('iiowatcher')
        self.log.addHandler(JournalHandler())
        self.log.setLevel(logging.DEBUG)

        self.output = output
        self.log.warning(f"Running on output {self.output}")

        self.set_main_loop()
        bus = dbus.SystemBus()
        self.proxy = bus.get_object(
            self.bus_name, '/net/hadess/SensorProxy'
        )
        self.iio_if = dbus.Interface(
            self.proxy, dbus_interface=self.bus_name
        )
        self.acc_if = dbus.Interface(
            self.proxy,
            dbus_interface=f'{self.bus_name}.AccelerometerOrientation'
        )

        self.props_if = dbus.Interface(
            self.proxy, 'org.freedesktop.DBus.Properties'
        )
        self.props_if.connect_to_signal(
            "PropertiesChanged", self.props_changed
        )
        self.log.info("dbus setup complete")

        signal.signal(signal.SIGTERM, self.cleanup)
        signal.signal(signal.SIGINT, self.cleanup)
        signal.signal(signal.SIGUSR1, self.toggle)

        self.claimed_accelerometer = False

    def claim_accelerometer(self):
        if not self.claimed_accelerometer:
            self.log.info("claiming accelerometer")
            self.iio_if.ClaimAccelerometer()
            self.claimed_accelerometer = True
        else:
            self.log.info("already claiming accelerometer")

    def release_accelerometer(self):
        if self.claimed_accelerometer:
            self.log.info("releasing accelerometer")
            self.iio_if.ReleaseAccelerometer()
            self.claimed_accelerometer = False
        else:
            self.log.info("not releasing accelerometer, never claimed")

    def set_main_loop(self):
        Gst.init(None)
        self.mainloop = GLib.MainLoop()
        DBusGMainLoop(set_as_default=True)

    def props_changed(self, sender, content, *args, **kwargs):
        self.log.debug(f"Received message from {sender}: ", content)
        if "AccelerometerOrientation" in content:
            self.accelerometer_cb(content["AccelerometerOrientation"])
        else:
            print(sender, content, args, kwargs)

    def toggle(self, *args):
        self.log.warning("Switching mode!")
        if self.claimed_accelerometer:
            self.release_accelerometer()
        else:
            self.claim_accelerometer()

    def cleanup(self, *args):
        self.log.warning("Cleaning up")
        self.release_accelerometer()

        if self.mainloop.is_running():
            self.log.warning("Killing main loop")
            self.mainloop.quit()

    def run(self):
        try:
            self.mainloop.run()
        except KeyboardInterrupt:
            self.log.warning("Exiting with ^C")
        except Exception as e:
            self.log.warning("Exiting", exc_info=e)
        finally:
            self.cleanup()

    def set_orientation(self, deg):
        self.log.info(f"Setting orientation to {deg}deg")
        proc = subprocess.Popen([
            "sway", "output", self.output, "transform", str(deg)
        ], stdout=subprocess.PIPE)

        ans = json.loads(proc.communicate()[0])
        if ans != [{'success': True}]:
            self.log.error("calling sway failed")
            self.log.error(ans)

    def accelerometer_cb(self, o):
        if o == "normal":
            self.set_orientation(0)
        elif o == "right-up":
            self.set_orientation(90)
        elif o == "left-up":
            self.set_orientation(270)
        elif o == "bottom-up":
            self.set_orientation(180)
        else:
            self.log.error(f"Unknown orientation {o}")


if __name__ == "__main__":
    self = IIOWatcher("eDP-1")
    self.run()
