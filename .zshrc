case `cat $HOME/.system` in
    surface*)
        c1="green"
        c2="blue"
        prefix=""
        ;;
    ippp*)
        c1="blue"
        c2="green"
        prefix="(IPPP) "
        export ATUIN_HOST_USER=$(whoami)
        ;;
    psi*)
        c1="blue"
        c2="green"
        prefix="(PSI) "
        ;;
    itp*)
        c1="blue"
        c2="green"
        prefix="(ITP) "
        ;;
    itpc*)
        c1="blue"
        c2="green"
        prefix="(cluster) "
        ;;
    phone*)
        c1="blue"
        c2="green"
        prefix="(pine)"
esac
export PROMPT="$prefix%B%F{$c1}%n@%m%f:%F{$c2}%.%b%f$ "
# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# some more ls aliases
alias ll='ls -alFctr'
alias la='ls -A'
alias l='ls -CF'
alias bat='bat --paging=never'

# My stuff

export BAT_THEME="GitHub"

case `cat $HOME/.system` in
    surface*)
        export GST_PLUGIN_PATH=/usr/local/lib/x86_64-linux-gnu/gstreamer-1.0
        export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/lib:/usr/lib:/usr/local/lib:/usr/local/lib/x86_64-linux-gnu/
        export PYTHONPATH=$PYTHONPATH:/home/yannickulrich/mcmule/pymule
        export PATH=$HOME/.local/math12.3:$PATH:$HOME/.local/bin:$HOME/.cargo/bin
        alias xournal=xournalpp
        alias ssh='/home/yannickulrich/.ssh/ssh-wrapper'
        alias scp='scp -S /home/yannickulrich/.ssh/ssh-wrapper'
        export MAIN_TTY=\#{pane_tty}
        $HOME/.ssh/get-agent.sh
        export SSH_AUTH_SOCK=$HOME/.ssh/ssh_auth_sock

        if dropbox status | grep -q 'running' ; then
            dropbox start > /dev/null 2> /dev/null &
        fi
        ;;
    psi*)
        . /afs/psi.ch/project/muondecay/env.sh
        PATH=$HOME/.local/bin:$PATH
        hostname=`hostname`
        if [ "$hostname" == "lcth32" ] ||  [ "$hostname" == "lcth31" ] ||  [ "$hostname" == "lcth22" ] ||  [ "$hostname" == "lcth21" ]; then
            alias kr='kinit -k -t /scratch/ulrich/.tab.keytab ulrich_y@D.PSI.CH ; aklog'
            alias krs='k5reauth -i 3600 -p ulrich_y -k /scratch/ulrich/.tab.keytab -f -- screen'
        elif [ "$hostname" == "pc13590.psi.ch" ] || [ "$hostname" == "pc10634.psi.ch" ]; then
            export PATH=/scratch/ulrich/usr/bin:$PATH
            export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/scratch/ulrich/usr/lib
            export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:/scratch/ulrich/usr/lib/pkgconfig
            alias kr='kinit -k -t /scratch/ulrich/.tab.keytab ulrich_y@D.PSI.CH ; aklog'
            alias krs='k5reauth -i 3600 -p ulrich_y -k /scratch/ulrich/.tab.keytab -f -- screen'
        fi
        PATH=/afs/psi.ch/sys/psi.x86_64_slp6/Tools/git/2.33.1/bin:$PATH
        export MAIN_TTY=$SSH_TTY
        ;;
    ippp*)
        export MAIN_TTY=$SSH_TTY
        source /opt/rh/rh-python38/enable
        source /opt/rh/devtoolset-10/enable
        export PATH=$HOME/.local/bin/:$PATH
        ;;
    itp*)
        export MAIN_TTY=$SSH_TTY
        source /space2/hepsw/python/3.12.0/enable
        export RUSTUP_HOME=//space4/schalchn/rust/rustup
        export PATH=$HOME/.local/bin/://space4/schalchn/rust/cargo/bin:$PATH
        ;;
esac

alias colourdiff=colordiff
export EDITOR=vim
alias config='git --git-dir=$HOME/.cfg/ --work-tree=$HOME'

alias clear="/usr/bin/clear -T tmux"


function gitignore (){
    IFS=","
    req="$*"
    curl "https://www.toptal.com/developers/gitignore/api/$req" > .gitignore
    git add .gitignore
}

# eval "$(atuin init zsh)"
source $HOME/.config/atuin/zsh.sh

# The following lines were added by compinstall
zstyle :compinstall filename '/home/yannickulrich/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall
# Lines configured by zsh-newuser-install
setopt autocd notify
unsetopt nomatch
bindkey -e
# End of lines configured by zsh-newuser-install


# foot specific
if [[ "$TERM" == "foot" ]]; then
    precmd() {
        print -Pn "\e]133;A\e\\"
    }
    add-zsh-hook precmd precmd
    bindkey '^[[H'    beginning-of-line
    bindkey '^[[F'    end-of-line
    bindkey '^[[3~'   delete-char
else
    bindkey  "${terminfo[khome]}"    beginning-of-line
    bindkey  "${terminfo[kend]}"     end-of-line
    bindkey  "${terminfo[kdch1]}"    delete-char
fi
