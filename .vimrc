call plug#begin('~/.vim/plugged')
Plug 'tpope/vim-commentary'
Plug 'bling/vim-bufferline'
Plug 'will133/vim-dirdiff'
Plug 'rsmenon/vim-mathematica'
Plug 'git@gitlab.com:yannickulrich/oscclip/', { 'branch': 'vim' }
Plug 'maxbrunsfeld/vim-yankstack'
Plug 'psf/black', { 'branch': 'stable' }
call plug#end()
let g:black_use_virtualenv = 0

map h <Nop>
noremap ; <Right>
noremap l <Up>
noremap k <Down>
noremap j <Left>

" use Vim defaults instead of 100% vi compatibility
set nocompatible

" more powerful backspacing
set backspace=indent,eol,start

" line numbers: relative and absolute
set relativenumber
set number

" set tabcomplete for files
set wildmode=longest,list,full
set wildmenu

" store swap files centrally
set directory^=$HOME/.vim/swap//

" keep buffer history in closed splits
set hidden

if has("gui_running")
  " empty GUI of crap
  set guioptions=egimrLtak
  set tb=
else
  " cursor shape
  let &t_SI = "\e[5 q"
  let &t_EI = "\e[2 q"

  " activate mouse
  set mouse=a
  set ttymouse=sgr

  " fix paste with ctrl+v
  let &t_SI .= "\<Esc>[?2004h"
  let &t_EI .= "\<Esc>[?2004l"

  inoremap <special> <expr> <Esc>[200~ XTermPasteBegin()

  function! XTermPasteBegin()
    set pastetoggle=<Esc>[201~
    set paste
    return ""
  endfunction

endif

function! SetShift(n)
  set expandtab
  exec "set tabstop=".a:n
  exec "set shiftwidth=".a:n
  exec "set softtabstop=".a:n
endfunction

call SetShift(4)

" title string?!
let &titlestring = @%
set title

" highlight all search matches
set hlsearch

" automatic file type detection: loads settings from ftplugin/
filetype plugin on

" show current position
set ruler

" Add words to dict
set dictionary+=/usr/share/dict/words

let g:word_count=wordcount().words
function WordCount()
    if has_key(wordcount(),'visual_words')
        let g:word_count=wordcount().visual_words."/".wordcount().words " count selected words
    else
        let g:word_count=wordcount().cursor_words."/".wordcount().words " or shows words 'so far'
    endif
    return g:word_count
endfunction

function ActivateWordCount()
    set statusline+=\ w:%{WordCount()},
    set laststatus=2 " show the statusline
endfunction

nmap <S-T> :setlocal spell spelllang=en_gb<CR>:set tw=70<CR>


highlight ExtraWhitespace ctermbg=red guibg=red
autocmd Syntax * syn match ExtraWhitespace /\s\+$\| \+\ze\t/
