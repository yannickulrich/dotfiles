ATUIN_SESSION=$(atuin uuid)
stty_orig=$(stty -g)
export ATUIN_SESSION

_atuin_preexec() {
    local id
    id=$(atuin history start -- "$1")
    export ATUIN_HISTORY_ID="${id}"
}

_atuin_precmd() {
    local EXIT="$?"

    [[ -z "${ATUIN_HISTORY_ID}" ]] && return

    (RUST_LOG=error atuin history end --exit "${EXIT}" -- "${ATUIN_HISTORY_ID}" &) >/dev/null 2>&1
    stty_orig=$(stty -g)
}

__atuin_history() {
    tput rmkx
    # shellcheck disable=SC2048,SC2086
    HISTORY="$(RUST_LOG=error /home/yannickulrich/Cs/atuin/target/debug/atuin search $* -i -- "${READLINE_LINE}" 3>&1 1>&2 2>&3)"
    tput smkx

    READLINE_LINE=${HISTORY}
    READLINE_POINT=${#READLINE_LINE}
}

__atuin_myhistory() {
    tput rmkx
    # shellcheck disable=SC2048,SC2086
    HISTORY="$(RUST_LOG=error /home/yannickulrich/Cs/atuin/target/debug/atuin search $* -i -- "${READLINE_LINE}" 3>&1 1>&2 2>&3)"
    exitcode=$?
    tput smkx

    if [[ "$exitcode" == "0" ]]; then
        echo -e "${PS1@P}${HISTORY}"
        _atuin_preexec "${HISTORY}"
        local stty_bkup=$(stty -g)
        stty "$stty_orig"
        eval "${HISTORY}"
        _atuin_precmd
        stty "$stty_bkup"
    else
        READLINE_LINE=${HISTORY}
        READLINE_POINT=${#READLINE_LINE}
    fi
}

if [[ -n "${BLE_VERSION-}" ]]; then
    blehook PRECMD-+=_atuin_precmd
    blehook PREEXEC-+=_atuin_preexec
else
    precmd_functions+=(_atuin_precmd)
    preexec_functions+=(_atuin_preexec)
fi

bind -x '"\C-r": __atuin_history'
bind -x '"\e[A": __atuin_myhistory --search-mode prefix --shell-up-key-binding --inline-height 1'
bind -x '"\eOA": __atuin_myhistory --search-mode prefix --shell-up-key-binding --inline-height 1'
