set -e

width=20

function sendemoji {
    read line
    wtype ${line:0:1}
}

sed '0,/^__DATA__$/d' "$0" | while read line ; do
    IFS=","
    for token in ${line::-1} ; do
        echo ${line: -1} $token
        # printf "%-${width}s %s\n" "$token" ${line: -2}
    done
done | wmenu | sendemoji

exit
__DATA__
laughing loud             😆
:D,laughing               😄
grin,grinning,:|          😁
blush                     😊
sweat smile,XD            😅
joy                       😂
rolling on the floor,rofl 🤣
heart eyes                😍
star struck               🤩
tongue out,:P             😋
smiling face with tear    🥲
thinking,hmm              🤔
no mouth,:#               😶
relieved                  😌
sleepy                    😪
sleeping                  😴
confused,:/               😕
frown,:(                  ☹️
:),slight_smile           🙂
open_mouth,:0             😮
sad                       😥
cry                       😢
sob                       😭
scream                    😱
rage                      😡
yawn                      🥱
fingers_crossed           🤞
thumbs-up,+1              👍
face_palm                 🤦
purple_heart              💜
party                     🥳
skull                     ☠️
GBP                       £
