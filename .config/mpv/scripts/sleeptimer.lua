--- Sleeptimer.lua: Simple Sleeptimer for MPV
local msg = require "mp.msg"
local utils = require "mp.utils"
local options = require "mp.options"

-- Basic Functions
function osd(str, sec)
	return mp.osd_message(str, sec)
end

-- Start Sleeptimer
function sleeptimer_start()
	if time_duration then
		time_now = os.time()
		time_pressed_diff = os.difftime(time_now, time_pressed)
		if time_pressed_diff < 3 then
			time_pressed = os.time()
			time_duration = time_duration + 60
			osd(string.format("Timer-Duration: Increased to %s seconds.",
				time_duration), 1)
		else
			local time_now = os.time()
			local time_start_diff = os.difftime(time_now, time_start)
		end
	else
		time_start = os.time()
		time_pressed = os.time()
		time_duration = 60
		osd(string.format("Timer-Duration: Started at %s seconds.", time_duration), 1)
	end
end

-- Create Loop-Timer via MPV function
loop_timer = mp.add_periodic_timer(1, function()
	if time_duration then
		local time_now = os.time()
		local time_start_diff = os.difftime(time_now, time_start)
		local time_left_diff = time_duration - time_start_diff

		if time_left_diff <= 0 then
			osd(string.format("Time-Duration: Time is up!"), 10)
		    mp.command("quit")
        else
			osd(string.format("Timer-Duration: %s seconds left.", time_left_diff), 1)
		end
	end
end)

-- TODO: Choose better Keybinding which doesn't overwrite anything default.
mp.add_key_binding("t", "sleeptimer", sleeptimer_start)
