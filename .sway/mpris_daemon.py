#!/usr/bin/python3
# inspired from https://github.com/airtower-luna/mpris-python
# Copyright (c) 2015-2021 Fiona Klute (MIT Licence)

import dbus
from dbus.mainloop.glib import DBusGMainLoop
import time
import signal
import logging
from systemd.journal import JournalHandler


class Mpris:
    mpris_base = 'org.mpris.MediaPlayer2'
    player_interface = mpris_base + '.Player'
    properties_interface = 'org.freedesktop.DBus.Properties'
    _alive = True

    def __init__(self, name, monitor=False):
        self.log = logging.getLogger('mpris.Mpris')
        self.log.addHandler(JournalHandler())
        self.log.setLevel(logging.WARN)

        self.log.info(f"Connecting to {name} (monitor={monitor})")

        bus = dbus.SessionBus()

        self.proxy = bus.get_object(name, '/org/mpris/MediaPlayer2')
        self.player_iface = dbus.Interface(self.proxy, dbus_interface=self.player_interface)
        self.props_iface = dbus.Interface(self.proxy, dbus_interface=self.properties_interface)

        self.play = self.player_iface.Play
        self.pause = self.player_iface.Pause
        self.toggle = self.player_iface.PlayPause
        self._last_changed = time.time()

        if monitor:
            self.props_iface.connect_to_signal(
                'PropertiesChanged', self.cb
            )

    def get(self, arg):
        ans = self.props_iface.Get(self.player_interface, arg)
        self.log.info(f"Getting {arg}: {str(ans)}")
        return ans

    @property
    def playing(self):
        return self.get("PlaybackStatus") == "Playing"

    def cb(self, iface_name, changed, invalidated):
        if iface_name != self.player_interface:
            return

        self.log.info(f"Got callback, changed {str(changed)}")
        if 'PlaybackStatus' not in changed:
            return

        if changed['PlaybackStatus'] == 'Stopped':
            self.log.info("Quitting")
            self._alive = False
        self._playing = changed['PlaybackStatus'] == "Playing"
        self._last_changed = time.time()


class Daemon:
    children = []

    def __init__(self):
        self.log = logging.getLogger('mpris.Daemon')
        self.log.addHandler(JournalHandler())
        self.log.setLevel(logging.INFO)

        import gi
        gi.require_version('Gst', '1.0')
        from gi.repository import Gst, GLib

        Gst.init(None)
        self.mainloop = GLib.MainLoop()
        DBusGMainLoop(set_as_default=True)

        self.bus = dbus.SessionBus()

        self.proxy = self.bus.get_object('org.freedesktop.DBus', '/org/freedesktop/DBus')
        self.iface = dbus.Interface(self.proxy, dbus_interface='org.freedesktop.DBus')
        self.iface.connect_to_signal('NameOwnerChanged', self.cb)

        signal.signal(signal.SIGUSR1, self.handler)

        self.list()

    def list(self):
        for service in self.bus.list_names():
            if 'org.mpris.MediaPlayer2' in service:
                self.children.append(Mpris(service, True))
                self.log.info(f"Connecting to service {service}")

    def cb(self, iface_name, changed, invalidated):
        if 'org.mpris.MediaPlayer2' in iface_name and changed == '':
            try:
                self.log.info(f"New service {iface_name}")
                self.children.append(Mpris(iface_name, True))
            except dbus.exceptions.DBusException:
                pass
        else:
            n = len(self.children)

            self.children = [i for i in self.children if i._alive]
            self.log.info(f"Children {n} -> {len(self.children)}")

    def toggle(self):
        if len(self.children) == 0:
            return
        elif any(i.playing for i in self.children):
            self.log.info("Received signal, pausing all {len(self.children)} children")
            for i in self.children:
                i.pause()
        else:
            child = max(self.children, key=lambda child : child._last_changed)
            self.log.info("Received signal, playing {str(child)}")
            child.play()

    def handler(self, signum, stack):
        self.toggle()

    def run(self):
        self.mainloop.run()


if __name__ == "__main__":
    Daemon().run()
