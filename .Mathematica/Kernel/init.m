(** User Mathematica initialization file **)

AppendTo[$Path, "/home/yannickulrich/Sc/qgraf/"];

Unprotect[Information]
DownValues[Information] = {
    HoldPattern[Information[System`Dump`s_Symbol, System`Dump`opts___]] :> (
        Information[#1, System`Dump`opts] &
    )[ToString[Unevaluated[System`Dump`s], InputForm]],

    HoldPattern[Information[
        System`Dump`s_?System`Dump`validStringExpressionQ,
        System`Dump`opts___?OptionQ /; {} === Complement[
            First /@ Flatten[{System`Dump`opts}], {LongForm}
        ]
    ]] :> Module[{
        System`Dump`targetnb = EvaluationNotebook[],
        System`Dump`names,
        System`Dump`matchnames,
        System`Dump`lf,
        System`Dump`randtag = System`Dump`generaterandomtag[]
    },
        If[System`Dump`specialCharacterPatternQ[System`Dump`s],
            Return[System`Dump`specialCharacterInformation[System`Dump`s, System`Dump`opts]]
        ];
        System`Dump`names = System`Dump`splitstring[System`Dump`s];
        System`Dump`matchnames = (
            Names[#1, IgnoreCase -> False, SpellingCorrection -> False] &
        ) /@ System`Dump`names /. _Names -> {};
        System`Dump`matchnames = Map[
            {StringSplit[#1, "`"][[-1]], Context[#1]} &,
            System`Dump`matchnames, {2}
        ];

        If[System`Dump`names === {},
            System`Dump`names = {System`Dump`s}
        ];
        If[System`Dump`matchnames === {},
            System`Dump`matchnames = {{}}
        ];
        {System`Dump`lf} = {LongForm} /. Flatten[{System`Dump`opts, Options[Information]}];
        MapThread[
            System`Dump`outputinfo[
                System`Dump`targetnb,
                System`Dump`randtag,
                System`Dump`generateinfocells[
                    System`Dump`targetnb, #1, #2,
                    System`Dump`randtag, System`Dump`lf,
                    "FormattedUsage" /. System`Dump`$extendedmessageoptions,
                    "FormattedDefinition" /. System`Dump`$extendedmessageoptions
                ]
            ] &,
            {
                System`Dump`names /. HoldPattern -> HoldForm,
                System`Dump`matchnames
            }
        ];
        If[Length[$Urgent] > 1,
            Block[{$Urgent = DeleteCases[$Urgent, "stdout" | OutputStream["stdout", ___]]},
                Information[System`Dump`s, System`Dump`opts]
            ]
        ];
    ] /; Head[$FrontEnd] === FrontEndObject &&
         First[$FrontEnd] === $ParentLink &&
         ! FreeQ[$Urgent, "stdout"] &&
         TrueQ["UsageButtons" /. System`Dump`$extendedmessageoptions]
};
Protect[Information];
Get[ToFileName["/home/yannickulrich/.Mathematica/Autoload","init.m"]]
